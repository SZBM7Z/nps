import boto3
import os
import pandas as pd 
import csv
from subprocess import call


def lambda_handler (event, context):

    # Download excel files from S3 
    s3 = boto3.resource('s3')
    s3.meta.client.download_file('szbm7z-nps-104964-lego-us-east-1-nps-test', 'Cloud Dojo Completion _ Feedback.xlsx', '/tmp/dojo.xlsx')
    s3.meta.client.download_file('szbm7z-nps-104964-lego-us-east-1-nps-test', 'TOCE Office Hours _ Please rate.xlsx', '/tmp/officehours.xlsx')
    
    # DEBUG - check to see if file is there, will return True if file is present
    # print(os.path.isfile('/tmp/dojo.xlsx')) 
    # print(os.path.isfile('/tmp/officehours.xlsx'))

    # Optional display settings for pandas when printing the excel in console
    # pd.set_option('display.max_rows', None) 
    # pd.set_option('display.max_columns', None)    
    # pd.set_option('display.width', 5000) 

    ####DOJO####

    #Open excel file, only use column F (this is the column for the question about if you would recommend this service to a friend, and skip row 1 (those were test data points from Glenn)
    df=pd.read_excel('dojo.xlsx', usecols = "F", skiprows = 1)   

    # DEBUG - check and see if got the right column / data
    # print(df)

    # Init vars for dojo NPS calculations
    dojoPromoters = 0
    dojoDetractors = 0
    dojoPassives = 0
    dojoNPS = 0

    # Loop through the rows of the excel and track how many promoter, passivies, and detractors
    # i = index, j = customer rating
    for i, j in df.iterrows():
        if j.item() >= 9:
            dojoPromoters = dojoPromoters + 1 
        elif 7 <= j.item() <= 8:
            dojoPassives = dojoPassives + 1
        elif  j.item() <= 6:
            dojoDetractors = dojoDetractors + 1
    
    # DEBUG - print out count of detractors, promoters, passives. Good to compare with what's in Msft Forms
    # print("detractor: " , dojoDetractors)
    # print("promoter: " , dojoPromoters)
    # print("passive: "  , dojoPassives)

    # Calc total of responses and NPS score
    # How to calculate NPS = https://delighted.com/nps-calculator
    dojoTotal =  dojoDetractors + dojoPromoters + dojoPassives
    dojoNPS = (dojoPromoters - dojoDetractors)  / dojoTotal * 100  
    
    # DEBUG - print out NPS score
    # print("dojo NPS: ", int(dojoNPS))


   ####Office Hours#### 

    #Open excel file, only use column F (this is the column for the question about if you would recommend this service to a friend, and skip rows 2 and 3 (those were test data points from Gayle)
    df=pd.read_excel('officehours.xlsx', usecols = "F", skiprows = [2,3])   

    # DEBUG if you got the correct rows to "skiprows"
    # print(df) 

    #init variables 
    officeHoursPromoters = 0
    officeHoursDetractors = 0
    officeHoursPassives = 0
    officeHoursNPS = 0

    # Loop through rows in the excel and catagorize the score given
    for i, j in df.iterrows():
        if j.item() >= 9:
            officeHoursPromoters = officeHoursPromoters + 1 
        elif 7 <= j.item() <= 8:
            officeHoursPassives = officeHoursPassives + 1
        elif  j.item() <= 6:
            officeHoursDetractors = officeHoursDetractors + 1
    
    # DEBUG print statments - good to compare with the results in the Microsoft Forms results summary page 
    print("OH detractor: " , officeHoursDetractors)
    print("OH promoter: " , officeHoursPromoters)
    print("OH passive: "  , officeHoursPassives)

    # Total up the number of respones and use that to calculate NPS score as a percentage 
    totalOfficeHours =  officeHoursDetractors + officeHoursPassives + officeHoursPromoters
    officeHoursNPS = (officeHoursPromoters - officeHoursDetractors) / totalOfficeHours * 100 

    # Another debug print statement
    print("OH NPS: ", int(officeHoursNPS))

    avgNPS = (officeHoursNPS + dojoNPS) / 2 

     # Another debug print statement
    print("AVG NPS: ", int(avgNPS))

    # Create a new temp csv file and write the AVG NPS score to it
    f = open("/tmp/nps.csv", "w+")
    temp_csv_file = csv.writer(f) 
    temp_csv_file.writerow(["NPS", avgNPS])
    f.close()
    
    s3.meta.client.upload_file('/tmp/nps.csv', 'szbm7z-nps-104964-lego-us-east-1-nps-test', 'nps.csv', ExtraArgs={'ServerSideEncryption': "aws:kms"})

    # Clean up - remove tmp files 
    call('rm -rf /tmp/*', shell=True) 

    # Check and see if they still exist
    print(os.path.isfile('/tmp/dojo.xlsx')) 
    print(os.path.isfile('/tmp/nps.csv'))


def main(): 
    lambda_handler("","")

if __name__ == "__main__":
    main()