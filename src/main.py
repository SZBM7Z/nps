import boto3
import os
import tempfile
import json
import office365

from office365.sharepoint.files.file import File
from office365.runtime.auth.user_credential import UserCredential
from office365.sharepoint.client_context import ClientContext


def lambda_handler (event, context):
    site_url = "https://aticsi.sharepoint.com/sites/Ally-CloudEnablement/"

    sm_client = boto3.client('secretsmanager')
    response = sm_client.get_secret_value(
        SecretId='szbm7z-nps-104964-lego-sharepoint'
    )
    secret = json.loads(response['SecretString'])


    ctx = ClientContext(site_url)
    ctx = ClientContext(site_url).with_credentials(UserCredential(secret['email'],secret['pw']))    
    target_web = ctx.web.get().execute_query()
    print(target_web.url)
    
    

    # got this link from clicking the "share" button for the dojo excel sheet in sharepoint
    # file = ctx.web.get_file_by_guest_url('https://aticsi.sharepoint.com/:x:/s/Ally-CloudEnablement/EVvNJ11iP0BFuQidB-9Xy6sBRpQd8Qs5HPafK8H0RfptnQ?e=1ffvsI').execute_query()
    # file = ctx.web.get_file_by_guest_url('hhttps://forms.office.com/Pages/AnalysisPage.aspx?id=gXGia9D56ku9aNq3UwsVqnTdBc4IvTtLjhot3djXED5UOVZIM1RVOUZITVhJM0RMWTMxNjdGNlg3VSQlQCN0PWcu&AnalyzerToken=1oP6vQSnJdku1JxWaFJREOCr4OhTZrJP').execute_query()

    # # print all file properties; used this to figure out what the paths/file urls are 
    # print(json.dumps(file.properties))

    # abs_file_url = 'https://aticsi.sharepoint.com/sites/Ally-CloudEnablement/Shared Documents/Cloud Dojo Completion _ Feedback.xlsx'
    # relative_path = '/sites/Ally-CloudEnablement/Shared Documents/Cloud Dojo Completion _ Feedback.xlsx'


    #Below is working locally, had to make sure the path you want to store the file is existing. For my case, I had to create a dojoStuff folder and then it worked when I ran it. 
    # file_name = os.path.basename(abs_file_url)
    # temp = os.path.normpath(os.path.join(r'/tmp/', file_name))
    # temp2 = os.path.join(r'/tmp/', file_name)
    # print(temp)
    # print(temp2)
    # with open(temp, 'wb+') as local_file:
    #     # file = File.from_url(abs_file_url3).with_credentials(UserCredential("ann.smith2@ally.com", os.environ.get("PROXY_PASSWD"))).download(local_file).execute_query()
    #     file = ctx.web.get_file_by_server_relative_path(relative_path).download(local_file).execute_query()
    #     print(file.properties)
    #     print("'{0}' file has been downloaded into {1}".format(file.serverRelativeUrl, local_file.name))
    print('done!!!')


def main(): 
    lambda_handler("","")

if __name__ == "__main__":
    main()