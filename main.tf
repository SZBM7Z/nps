data "aws_caller_identity" "current" {}

module "namespace" {
  source              = "git::ssh://git@bitbucket.int.ally.com/tf/terraform-modules-name-and-tags.git?ref=v1"
  application_name    = var.application_name
  service_name        = var.service_name
  workspace           = terraform.workspace
  environment         = var.environment
  application_id      = var.application_id
  data_classification = var.data_classification
  issrcl_level        = var.issrcl_level
  owner               = var.owner
  scm_project         = var.scm_project
  scm_repo            = var.scm_repo
  scm_branch          = var.scm_branch
  scm_commit_id       = var.scm_commit_id
  additional_tags = {
    "tf_starter" = var.creation_date
  }
}

# Documentation: https://bitbucket.int.ally.com/projects/TF/repos/terraform-modules-aws-lambda/browse
module "lambda" {
  source      = "git::ssh://git@bitbucket.int.ally.com/tf/terraform-modules-aws-lambda.git?ref=v6"
  namespace   = module.namespace.lower_resource_name
  name        = "nps"
  environment = var.environment

  runtime       = "python3.7"
  vpc_cidr_code = lookup(var.awsacct_cidr_code, var.environment)
  tags          = module.namespace.tags
  short_tags    = module.namespace.short_tags
  layer_arns    = [aws_lambda_layer_version.lambda_layer.arn]
  handler = "main.lambda_handler"
  execution_policy = data.aws_iam_policy_document.policy.json
  code = "${path.module}/src"
  size = "s"
  env_vars = {
    # BUCKET = module.s3.arn
    # SVC_ACCOUNT = "arn:aws:secretsmanager:us-east-1:235829081928:secret:renovate-bot-104284-default-secret-aZ1lsO"
    ACCOUNT = module.sharepoint_creds.secret.id
  }
}

module "calculate_lambda" {
  source      = "git::ssh://git@bitbucket.int.ally.com/tf/terraform-modules-aws-lambda.git?ref=v6"
  namespace   = module.namespace.lower_resource_name
  name        = "calculate"
  environment = var.environment
  execution_policy = data.aws_iam_policy_document.test_policy.json
  runtime       = "python3.7"
  vpc_cidr_code = lookup(var.awsacct_cidr_code, var.environment)
  tags          = module.namespace.tags
  short_tags    = module.namespace.short_tags
  handler = "main.lambda_handler"
  code = "${path.module}/code"
  size = "m"
  layer_arns    = [aws_lambda_layer_version.lambda_layer.arn]
}

data "aws_iam_policy_document" "test_policy" {
  statement {
    sid = "ReadS3"
    effect = "Allow"
    actions = [
      "s3:*",
      "s3:PutObject"
    ]
    resources = [
      "*"
    ]
  }
  statement {
    sid = "AllowLambdaToUseKMS"
    effect = "Allow"
    actions = [
      "kms:Decrypt",
      "kms:Encrypt",
      "kms:GenerateDataKey"
    ]
    resources = [
      "*"
    ]
  }
}

resource "aws_kms_key" "encryption" {
  description              = "Bucket encryption key"
  key_usage                = "ENCRYPT_DECRYPT"
  customer_master_key_spec = "SYMMETRIC_DEFAULT"
  deletion_window_in_days  = 7
  is_enabled               = true
  enable_key_rotation      = true
  tags                     = module.namespace.tags
}

#  Simple example - just a bucket
module "s3" {
  source    = "git::ssh://git@bitbucket.int.ally.com/tf/terraform-modules-aws-s3.git?ref=v2"
  namespace = module.namespace.lower_resource_name
  name      = "nps-test"
  tags      = module.namespace.tags
  kms_master_key_arn = aws_kms_key.encryption.arn
  versioning_enabled = false
}


resource "aws_s3_bucket_object" "dojo" {
  key        = "Cloud Dojo Completion _ Feedback.xlsx"
  bucket     = module.s3.id
  source     = "${path.module}/Cloud Dojo Completion _ Feedback.xlsx"
  kms_key_id = aws_kms_key.encryption.arn
}


module "sharepoint_creds" {
  source                  = "git::ssh://git@bitbucket.int.ally.com/tf/terraform-modules-aws-secretsmanager.git?ref=v3"
  description             = ""
  environment             = var.environment
  kms_key_arn             = module.encryption.key.arn
  name                    = "sharepoint"
  namespace               = module.namespace.lower_resource_name
  tags                    = module.namespace.tags
  recovery_window_in_days = 0 # For actual secrets, be sure to set your recovery window to at least 7 days
}

module "encryption" {
  source     = "git::ssh://git@bitbucket.int.ally.com/tf/terraform-modules-aws-encryption.git?ref=v1"
  name       = "cmk"
  namespace  = module.namespace.lower_resource_name
  tags       = module.namespace.tags
  encryptors = []
  decryptors = []
}



resource "aws_lambda_layer_version" "lambda_layer" {
  filename   = "${path.module}/sharepoint/python.zip"
  layer_name = module.namespace.lower_resource_name
 
}

data "aws_iam_policy_document" "policy" {
  statement {
    sid = "AllowLambdaToReadSecrets"
    effect = "Allow"
    actions = [
      "secretsmanager:GetSecretValue"
    ]
    resources = [
      # "arn:aws:secretsmanager:us-east-1:235829081928:secret:renovate-bot-104284-default-secret-aZ1lsO"
      module.sharepoint_creds.secret.arn
    ]
  }
  statement {
    sid = "AllowLambdaToUseKMS"
    effect = "Allow"
    actions = [
      "kms:Decrypt",
      "kms:Encrypt"
    ]
    resources = [
      "*"
    ]
  }
}

# Documentation: https://bitbucket.int.ally.com/projects/TF/repos/terraform-modules-aws-s3/browse
# module "s3" {
#   source    = "git::ssh://git@bitbucket.int.ally.com/tf/terraform-modules-aws-s3.git?ref=v2"
#   namespace = module.namespace.lower_resource_name
#   name      = "nps"

#   tags = module.namespace.tags
# }


