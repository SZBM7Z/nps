terraform {
  backend "s3" {
    bucket         = "ally-us-east-1-235829081928-tf-states"
    key            = "104964-nps-szbm7z/terraform.tfstate"
    dynamodb_table = "ally-us-east-1-235829081928-tf-locks"
    region         = "us-east-1"
    encrypt        = true
  }
}

