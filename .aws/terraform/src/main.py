import site
import boto3
import os
import tempfile
import json
import office365

from office365.sharepoint.files.file import File
from office365.runtime.auth.user_credential import UserCredential
from office365.runtime.auth.client_credential import ClientCredential
from office365.runtime.auth.authentication_context import AuthenticationContext
from office365.sharepoint.client_context import ClientContext
from office365.runtime.auth.token_response import TokenResponse
import msal
import requests

def lambda_handler (event, context):
    site_url = "https://aticsi.sharepoint.com/sites/Ally-CloudEnablement/"   
    sm_client = boto3.client('secretsmanager', region_name='us-east-1')
    response = sm_client.get_secret_value(
        # SecretId='nps_sharepoint_poc'
        SecretId=a
    )
    secret = json.loads(response['SecretString'])

    response2 = sm_client.get_secret_value(
        # SecretId='szbm7z-nps-104284-default-sharepoint'
        SecretId=b
    )
    cert = response2['SecretString']
    
    cert_settings = {
        'tenant': secret['tenant_id'],
        'client_id': secret['client_id'],
        'thumbprint': secret['thumbprint'],
        'resource': 'https://aticsi.sharepoint.com',
        'private_key': cert
    }
    authority_url = 'https://login.microsoftonline.com/{0}'.format(cert_settings.get('tenant'))
    credentials = {
        "thumbprint": secret['thumbprint'],
        "private_key": cert,
        "passphrase": secret['cert_passphrase']
    }
    
    scopes = ["{url}/.default".format(url=cert_settings.get('resource'))]
    
    app = msal.ConfidentialClientApplication(
        cert_settings.get('client_id'),
        authority=authority_url,
        client_credential=credentials,
    )
    result = app.acquire_token_for_client(scopes)
    return result['access_token']   
    # return TokenResponse.from_json(result)
    
# def upload_file(self, path, filename):
#     token = self.get_token()
#     url = "{}/GetFolderByServerRelativePath(DecodedUrl='{}')/Files/AddUsingPath(DecodedUrl='{}',overwrite=true)?$Select=ServerRelativeUrl".format(self.api_url, path, filename)
#     # file is written to data/filename - add that here
#     with open('{}/data/{}'.format(os.getcwd(), filename), 'rb') as f:
#         content = f.read()
#     headers = {
#         "accept": "application/json;odata=verbose",
#         'Authorization': 'Bearer {}'.format(token)
#     }
#     print(url)
#     r = requests.post(url, data=content, headers=headers, verify=False )
#     print(r.status_code)
#     print(r.text)
#     print(json.dumps(r.json(), indent=4))

def download_file(token, src_relative_path, dest_path):
    headers = {
        "accept": "application/json;odata=verbose",
        'Authorization': 'Bearer {}'.format(token)
    }
    url = "https://aticsi.sharepoint.com/sites/Ally-CloudEnablement/_api/web/GetFileByServerRelativeUrl('{}')/$value".format(src_relative_path)

    r = requests.get(url,headers=headers)  
    
    if r.status_code == 200:
        f = open(dest_path, "wb+")
        f.write(r.content)
        f.close()
    else:
        print(f'ERROR GETTING FILE! download_file() function returned error code:{r.status_code}')

def main(): 
    lambda_handler("","")

if __name__ == "__main__":
    main()
