import boto3
import os
import pandas as pd 
import csv
from subprocess import call
import requests
import msal 
import json
from datetime import datetime

def get_file_path(token):
    headers = {
        'Authorization': 'Bearer {}'.format(token),
        'Accept' : 'application/json;odata=verbose'
    }
    url = "https://aticsi.sharepoint.com/sites/Ally-CloudEnablement/_api/web/lists/getByTitle('Documents')/Files"

    r = requests.get(
        url,
        headers=headers
    )  

def acquire_token(secrets_sm_arn,cert_sm_arn):
    sm_client = boto3.client('secretsmanager', region_name='us-east-1')
    response = sm_client.get_secret_value(
        # SecretId='nps_sharepoint_poc'
        SecretId=secrets_sm_arn
    )
    secret = json.loads(response['SecretString'])

    response2 = sm_client.get_secret_value(
        # SecretId='szbm7z-nps-104284-default-sharepoint'
        SecretId=cert_sm_arn
    )
    cert = response2['SecretString']
    
    cert_settings = {
        'tenant': secret['tenant_id'],
        'client_id': secret['client_id'],
        'thumbprint': secret['thumbprint'],
        'resource': 'https://aticsi.sharepoint.com',
        'private_key': cert
    }
    authority_url = 'https://login.microsoftonline.com/{0}'.format(cert_settings.get('tenant'))
    credentials = {
        "thumbprint": secret['thumbprint'],
        "private_key": cert,
        "passphrase": secret['cert_passphrase']
    }
    
    scopes = ["{url}/.default".format(url=cert_settings.get('resource'))]
    
    app = msal.ConfidentialClientApplication(
        cert_settings.get('client_id'),
        authority=authority_url,
        client_credential=credentials,
    )
    result = app.acquire_token_for_client(scopes)
    return result['access_token']    
    
# def upload_file(self, path, filename):
#     token = self.get_token()
#     url = "{}/GetFolderByServerRelativePath(DecodedUrl='{}')/Files/AddUsingPath(DecodedUrl='{}',overwrite=true)?$Select=ServerRelativeUrl".format(self.api_url, path, filename)
#     # file is written to data/filename - add that here
#     with open('{}/data/{}'.format(os.getcwd(), filename), 'rb') as f:
#         content = f.read()
#     headers = {
#         "accept": "application/json;odata=verbose",
#         'Authorization': 'Bearer {}'.format(token)
#     }
#     print(url)
#     r = requests.post(url, data=content, headers=headers, verify=False )
#     print(r.status_code)
#     print(r.text)
#     print(json.dumps(r.json(), indent=4))

def download_file(token, src_relative_path, dest_path):
    headers = {
        "accept": "application/json;odata=verbose",
        'Authorization': 'Bearer {}'.format(token)
    }
    url = "https://aticsi.sharepoint.com/sites/Ally-CloudEnablement/_api/web/GetFileByServerRelativeUrl('{}')/$value".format(src_relative_path)

    r = requests.get(url,headers=headers)  
    
    if r.status_code == 200:
        f = open(dest_path, "wb+")
        f.write(r.content)
        f.close()
    else:
        print(f'ERROR GETTING FILE! download_file() function returned error code:{r.status_code}')

def lambda_handler (event, context):

    # Authenticate to SharePoint and grab access token
    token = acquire_token('nps_sharepoint_poc','szbm7z-sp-104964-dev-f-sharepoint-sharepoint_cert')

    # Get today's date and format desired file paths to download to for dojo and OH feedback excel
    # Currently downloads to the Lambda's /tmp file system
    todaysDate = datetime.now()
    dojoFilePath = '/tmp/DojoFeedback_{}.xlsx'.format(todaysDate.strftime("%m-%d-%Y"))
    officeHoursFilePath = '/tmp/OfficeHoursFeedback_{}.xlsx'.format(todaysDate.strftime("%m-%d-%Y"))
    
    # Download the files
    download_file(token, '/sites/Ally-CloudEnablement/Shared Documents/Cloud Dojo Completion _ Feedback.xlsx', dojoFilePath )
    download_file(token,'/sites/Ally-CloudEnablement/Shared Documents/TOCE Office Hours _ Please rate.xlsx', officeHoursFilePath)

    # Download excel files from S3 
    # s3 = boto3.resource('s3')
    # s3.meta.client.download_file('szbm7z-nps-104964-lego-us-east-1-nps-test', 'Cloud Dojo Completion _ Feedback.xlsx', '/tmp/dojo.xlsx')
    # s3.meta.client.download_file('szbm7z-nps-104964-lego-us-east-1-nps-test', 'TOCE Office Hours _ Please rate.xlsx', '/tmp/officehours.xlsx')
    
    # DEBUG - check to see if file is there, will return True if file is present
    # print(os.path.isfile(dojoFilePath)) 
    # print(os.path.isfile(officeHoursFilePath))

    # Optional display settings for pandas when printing the excel in console
    # pd.set_option('display.max_rows', None) 
    # pd.set_option('display.max_columns', None)    
    # pd.set_option('display.width', 5000) 

    ####DOJO####

    # Open excel file, only use column F (this is the column for the question about if you would recommend this service to a friend, and skip row 1 (those were test data points from Glenn)
    df=pd.read_excel(dojoFilePath, usecols = "F", skiprows = 1)   

    # DEBUG - check and see if got the right column / data
    # print(df)

    # Init vars for dojo NPS calculations
    dojoPromoters = 0
    dojoDetractors = 0
    dojoPassives = 0
    dojoNPS = 0

    # Loop through the rows of the excel and track how many promoter, passivies, and detractors
    # i = index, j = customer rating
    for i, j in df.iterrows():
        if j.item() >= 9:
            dojoPromoters = dojoPromoters + 1 
        elif 7 <= j.item() <= 8:
            dojoPassives = dojoPassives + 1
        elif  j.item() <= 6:
            dojoDetractors = dojoDetractors + 1
    
    # DEBUG - print out count of detractors, promoters, passives. Good to compare with what's in Msft Forms
    print("Dojo promoter: " , dojoPromoters)
    print("Dojo passive: "  , dojoPassives)
    print("Dojo detractor: " , dojoDetractors)

    # Calc total of responses and NPS score
    # How to calculate NPS = https://delighted.com/nps-calculator
    dojoTotal =  dojoDetractors + dojoPromoters + dojoPassives
    dojoNPS = (dojoPromoters - dojoDetractors)  / dojoTotal * 100  
    
    # DEBUG - print out NPS score
    print("dojo NPS: ", int(dojoNPS))
 

   ####Office Hours#### 

    #Open excel file, only use column F (this is the column for the question about if you would recommend this service to a friend, and skip rows 2 and 3 (those were test data points from Gayle)
    df=pd.read_excel(officeHoursFilePath, usecols = "F", skiprows = [2,3])   

    # DEBUG if you got the correct rows to "skiprows"
    # print(df) 

    #init variables 
    officeHoursPromoters = 0
    officeHoursDetractors = 0
    officeHoursPassives = 0
    officeHoursNPS = 0

    # Loop through rows in the excel and catagorize the score given
    for i, j in df.iterrows():
        if j.item() >= 9:
            officeHoursPromoters = officeHoursPromoters + 1 
        elif 7 <= j.item() <= 8:
            officeHoursPassives = officeHoursPassives + 1
        elif  j.item() <= 6:
            officeHoursDetractors = officeHoursDetractors + 1
    
    # DEBUG print statments - good to compare with the results in the Microsoft Forms results summary page  
    print("OH promoter: " , officeHoursPromoters)
    print("OH passive: "  , officeHoursPassives)
    print("OH detractor: " , officeHoursDetractors)

    # Total up the number of respones and use that to calculate NPS score as a percentage 
    totalOfficeHours =  officeHoursDetractors + officeHoursPassives + officeHoursPromoters
    officeHoursNPS = (officeHoursPromoters - officeHoursDetractors) / totalOfficeHours * 100 

    # Another debug print statement
    print("OH NPS: ", int(officeHoursNPS))

    avgNPS = (officeHoursNPS + dojoNPS) / 2 

     # Another debug print statement
    print("AVG NPS: ", int(avgNPS))

    # Create a new temp csv file and write the AVG NPS score to it
    f = open("/tmp/nps.csv", "w+")
    temp_csv_file = csv.writer(f) 
    temp_csv_file.writerow(["NPS", avgNPS])
    f.close()
    
    s3 = boto3.resource('s3')
    s3.meta.client.upload_file('/tmp/nps.csv', os.environ['BUCKET'], 'nps.csv', ExtraArgs={'ServerSideEncryption': "aws:kms"})

    # Clean up - remove tmp files 
    call('rm -rf /tmp/*', shell=True) 


def main(): 
    lambda_handler("","")

if __name__ == "__main__":
    main()