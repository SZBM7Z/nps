# Name & Tagging
application_id      = "104964"
application_name    = "szbm7z"
service_name        = "nps"
owner               = "szbm7z"
data_classification = "Proprietary"
issrcl_level        = "Low"
scm_project         = ""
scm_repo            = ""

awsacct_cidr_code = {
  default = "010-073-064-000"
  dev     = "010-073-064-000"
  qa      = "010-072-128-000"
  cap     = "010-072-128-000"
  psp     = "010-072-144-000"
  prod    = "010-072-144-000"
}

